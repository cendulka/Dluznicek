// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyBHohQvgkbSczqKB0hUKlYCI1R0SPRx3uY",
    authDomain: "dluznicek-994f4.firebaseapp.com",
    databaseURL: "https://dluznicek-994f4.firebaseio.com/",
    projectId: "dluznicek-994f4",
    storageBucket: "dluznicek-994f4.appspot.com",
    messagingSenderId: "279297018479"
  }
};
