import { Routes } from '@angular/router';
import { HomeComponent } from './home/index';
import { LoginComponent } from './login/index';
import { RegisterComponent } from './register/index';
import { DebtFormComponent } from './debt-form/debt-form.component';
import { DebtDetailComponent } from './debt-detail/debt-detail.component';


export const routes: Routes = [
  { path: '',       component: LoginComponent },
  { path: 'login',  component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  {	path: 'my-debts', component: DebtFormComponent},
  { path: ':type/:id', component: DebtDetailComponent },
  { path: 'others-debts', component: DebtFormComponent },
  { path: 'home', component: HomeComponent},
  { path: '**', component: LoginComponent },
];