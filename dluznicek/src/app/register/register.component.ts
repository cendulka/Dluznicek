import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  errorMessage: string;
  successMessage: string;

  constructor(public authService: AuthService) {}

  /*signup() {
    this.authService.signup(this.email, this.password, this.username, this.name);
    this.username = this.password = this.email = this.name = this.passwordAgain = '';    
  }*/

  tryRegister(value){
     this.authService.doRegister(value)
     .then(res => {
       console.log(res);
       this.errorMessage = "";
       this.successMessage = "Your account has been created";
     }, err => {
       console.log(err);
       this.errorMessage = err.message;
       this.successMessage = "";
     })
   }


  ngOnInit() {}
}

      
      