export class Debt {
  id : string;
	who: string;
	dueDate: Date;
	creationDate: Date;
  	amount: number;
 	  name: string;
  	note: string;
  	subject: string;

   constructor(name: string, subject:string, amount: number, creationDate: Date, dueDate: Date, who:string, note: string) {
   	this.dueDate = dueDate;
   	this.name = name;
    this.who = who;
   	this.creationDate = creationDate;
   	this.amount = amount;
   	this.note = note;
   	this.subject = subject;
   } 


}
