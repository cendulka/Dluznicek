export class User {
    id: string;
    username: string;
    name: string;
    email: string;

   constructor(username: string, name: string, email: string) {
   	this.username = username;
   	this.name = name;
   	this.email = email;
   } 
   
}