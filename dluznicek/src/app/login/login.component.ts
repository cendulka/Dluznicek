import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  errorMessage: string;

  constructor(public authService: AuthService) {}

  tryLogin(value){
    this.authService.doLogin(value)
    .then(res => {
      
    }, err => {
      console.log(err);
      this.errorMessage = err.message;
    })
  }

  ngOnInit() {}

}
