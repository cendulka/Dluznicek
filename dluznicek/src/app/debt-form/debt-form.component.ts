import { Component, OnInit } from '@angular/core';
import { DebtService } from '../services/debt.service';
import { Observable } from 'rxjs/Observable';
import { Debt } from '../models/debt';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';

@Component({
selector: 'app-debt-form',
templateUrl: './debt-form.component.html',
styleUrls: ['./debt-form.component.css']
})

export class DebtFormComponent implements OnInit {

	successMessage: string;
	errorMessage: string;
	public debts: Debt[];
	url: string;
	title1: string;
	title2: string;
	constructor(private debtService: DebtService, private authService: AuthService, private router: Router) {
	}

	ngOnInit() {
		this.url = this.router.url;
		if (this.url.includes('my-debts')) {
			this.title1 = 'My debts'
		} else {
			this.title1 = 'Others debts'
		}
		this.title2 = 'Create debt';
		var path;
		let self = this;
		this.authService.getCurrentUserId().then(function(userId){
			path = '/debts/' + userId + self.router.url;
			var x = self.debtService.getDebts(path);
		    x.snapshotChanges().subscribe(item => {
		    	self.debts = [];
		      item.forEach(element => {
		        var savedDebt = element.payload.toJSON();
		        savedDebt["id"] = element.key;
		        self.debts.push(savedDebt as Debt);
		      });
			})
		})

	}

	onSubmit(form) {
		var path;
		let self = this;
		this.authService.getCurrentUserId().then(function(userId){
			path = '/debts/' + userId + self.router.url;

			if (form.value.id == null) {
				self.title2 = 'Create debt';
	     	 	self.debtService.saveDebt(form.value, path)
			}
	    	else {
	    		self.title2 = 'Update debt';
				self.debtService.updateDebt(form.value)
	    	}
		}).then(res => {
	      this.successMessage = "Debt was saved successfully";
	      form.reset();
	    }, err => {
	      console.log(err);
	      this.errorMessage = err.message;
	    })
	}

	onDelete(id) {
		this.debtService.deleteDebt(id);
	}

	onEdit(debt, debtFormInputs) {
		for (let key in debt) {
			if (debtFormInputs.controls[key] != undefined) {
				debtFormInputs.controls[key].setValue(debt[key]);
			}
		}
	}

}
