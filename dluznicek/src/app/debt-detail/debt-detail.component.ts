import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { Debt } from '../models/debt';
import { DebtService }  from '../services/debt.service';
import { Observable } from 'rxjs/Observable';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-debt-detail',
  templateUrl: './debt-detail.component.html',
  styleUrls: ['./debt-detail.component.css']
})
export class DebtDetailComponent implements OnInit {

  public debt: Observable<any>;

  constructor(
  private route: ActivatedRoute,
  private debtService: DebtService,
  private location: Location,
  private authService: AuthService,
  private router: Router
) {}

  ngOnInit() {
  	let self = this;
  	 this.authService.getCurrentUserId().then(function(userId){
			var path = '/debts/' + userId + self.router.url;
			self.debt = self.debtService.getDebt(path);
		})
  }

  getDebt() {
  	var id = this.route.snapshot.paramMap.get('id');
  	this.debtService.getDebt(id);
  	console.log(this.debt);
  }

  goBack() {
  	this.location.back();
  }

}
