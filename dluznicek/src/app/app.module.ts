import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { AngularFireStorageModule } from 'angularfire2/storage';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { environment } from '../environments/environment';
import { routes } from './app.routes';
import { RouterModule } from '@angular/router';
import { HomeComponent } from './home/index';
import { LoginComponent } from './login/index';
import { RegisterComponent } from './register/index';
import { FormsModule } from '@angular/forms';
import { AuthService } from './services/auth.service';
import { UserService } from './services/user.service';
import { DebtService } from './services/debt.service';
import { DebtFormComponent } from './debt-form/debt-form.component';
import { CompareDirective } from './directives/compare.directive';
import { EmailDirective } from './directives/email.directive';
import { DebtDetailComponent } from './debt-detail/debt-detail.component';


@NgModule({
  imports: [
    BrowserModule,
    AngularFireModule.initializeApp(environment.firebase), 
    AngularFirestoreModule, 
    AngularFireAuthModule, 
    AngularFireStorageModule, 
    RouterModule.forRoot(routes),
    FormsModule,
    AngularFireDatabaseModule,
  ],
  declarations: [ AppComponent, LoginComponent, HomeComponent, RegisterComponent, DebtFormComponent, CompareDirective, EmailDirective, DebtDetailComponent ],
  bootstrap: [ AppComponent ],
  providers: [
    AuthService, 
    UserService,
    DebtService,
  ]
})
export class AppModule {}