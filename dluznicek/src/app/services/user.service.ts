import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import { User } from '../models/user';

@Injectable()
export class UserService {

 private dbPath = '/users';
 
  usersRef: AngularFireList<User> = null;
 
  constructor(private db: AngularFireDatabase) {
    this.usersRef = db.list(this.dbPath);
  }
 
  saveUser(email: string, username: string, name: string): void{
  	let newUser = new User(username, name, email);
    this.usersRef.push(newUser);
  }

}
