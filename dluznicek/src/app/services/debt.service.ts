import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import { Debt } from '../models/debt';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';


@Injectable()
export class DebtService {

  debts: AngularFireList<Debt> = null;
 
  constructor(private db: AngularFireDatabase, private authService: AuthService, private router: Router) {
  }
 
  saveDebt(dict, path){
   return new Promise<any>((resolve, reject) => {
      for (let key in dict) {
        let myValue = dict[key];
        if (myValue == undefined) {
          dict[key] = '';
        }
      }
    	let newDebt = new Debt(dict.name, dict.dueDate, dict.creationDate, dict.amount, dict.note, dict.subject, dict.who);
      this.debts = this.db.list(path);
      this.debts.push(newDebt)
      .then(res => {
        console.log("Created");
        resolve(res);
      }, err => reject(err))

      })
  }

  getDebts(dbPath) {
    this.debts = this.db.list(dbPath);
    return this.debts;
  }

  updateDebt(debt) {
    return new Promise<any>((resolve, reject) => {
      this.debts.update(debt.id,
        {
          name: debt.name,
          subject: debt.subject,
          amount: debt.amount,
          creationDate: debt.creationDate,
          dueDate: debt.dueDate,
          who: debt.who,
          note: debt.note
      })
      .then(res => {
        console.log("Updated");

        resolve(res);
      }, err => reject(err))

      })


  }

  deleteDebt(id) {
    this.debts.remove(id);
  }

  getDebt(path) {
   return this.db.object(path).valueChanges();
  }

 /* getDbPath() {
    this.authService.getCurrentUserId().then(function(userId){
      var path = '/debts/' + userId + self.router.url;

      if (form.value.id == null)
          self.debtService.saveDebt(form.value, path)
        else
        self.debtService.updateDebt(form.value)
    })
  }*/

}
