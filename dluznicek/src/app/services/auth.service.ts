import { Injectable } from '@angular/core';

import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import { Router } from '@angular/router';
import { UserService } from '../services/user.service';

import { Observable } from 'rxjs/Observable';

@Injectable()
export class AuthService {
  user: Observable<firebase.User>;
  userId: string;

  constructor(private firebaseAuth: AngularFireAuth, private userService: UserService, private router: Router) {
    this.user = firebaseAuth.authState;
  }

  getCurrentUserId() {
    return new Promise<any>((resolve, reject) => {
        this.user.subscribe((auth) => {
        if (auth.uid !== null) {
            console.log("userId " + auth.uid);
            resolve(auth.uid);
        }
            
        })

      })
  }

  doRegister(value){
    return new Promise<any>((resolve, reject) => {
      firebase.auth().createUserWithEmailAndPassword(value.email, value.password)
      .then(res => {
        this.userService.saveUser(value.email, value.username, value.name);
        setTimeout(() => 
        {
            this.router.navigate(['/login']);
        },
        1000);
        resolve(res);
      }, err => reject(err))
    })
  }

  doLogin(value){
    return new Promise<any>((resolve, reject) => {
      firebase.auth().signInWithEmailAndPassword(value.email, value.password)
      .then(res => {
        resolve(res);
      }, err => reject(err))
    })
}

  doLogout(){
    return new Promise((resolve, reject) => {
      if(firebase.auth().currentUser){
        this.firebaseAuth.auth.signOut();
        this.router.navigate(['/login']);
        resolve();
      }
      else{
        reject();
      }
    });
}

}
